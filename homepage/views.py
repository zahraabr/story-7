from django.shortcuts import render, redirect
from .models import User
from .forms import UserForm

#Create your views here.
# def index(request):
# 	usernya = User.objects.all().values()
# 	if (request.method == 'POST'):
# 		form = UserForm(request.POST)
# 		if (form.is_valid()):
# 			return render(request, 'confirm.html', {'usernya': usernya, 'form':form})
# 			# form.save()
# 			# return redirect('/')
# 	else:
# 		form = UserForm()
# 	return render(request, 'index.html', {'usernya': usernya, 'form':form})

def index(request):
	if request.method == "POST":
		form = UserForm(request.POST)
		if (form.is_valid()):
			new_name = form.cleaned_data['name']
			new_status = form.cleaned_data['status']
			response = {'name':new_name, 'status':new_status}
			return render(request, 'confirm.html', response)
	else:
		form = UserForm()
	usernya = User.objects.all()
	response = {'usernya' : usernya}
	return render(request, 'index.html', response)


def confirm(request):
	if request.method == "POST":
		form = UserForm(request.POST)
		if (form.is_valid()):
			new_user = User()
			new_user.name = form.cleaned_data['name']
			new_user.status = form.cleaned_data['status']
			new_user.save()
			return redirect('homepage:index')

	else:
		form = UserForm()
	return render(request,'confirm.html')


