from django import forms
from .models import User

class UserForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput, required=True)
    # name = forms.CharField(widget=forms.TextInput(attrs={
    #     'class' : 'form-control',
    #     'placeholder' : "hey! what's your name?",
    #     'type' : 'text',
    #     'maxlength' : '50',
    #     'required' : True,
    #     'label' : '',
    # }))
    status = forms.CharField(widget=forms.TextInput, required=True)
    # status = forms.CharField(widget=forms.TextInput(attrs={
    #     'class' : 'form-control',
    #     'placeholder' : "how are you feeling today?",
    #     'type' : 'text',
    #     'maxlength' : '100',
    #     'required' : True,
    #     'label' : '',
    # }))
    class Meta:
        model = User
        fields = ['name', 'status']
        widgets = {'model': forms.TextInput()}
        type = 'text'
        required = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
