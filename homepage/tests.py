from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import index, confirm
from django.apps import apps
from .apps import HomepageConfig
from .forms import UserForm
from .models import User
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story7Test(TestCase):
    def test_URL_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_template_is_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_confirmation_page_url_template_and_function(self):
        response = Client().get('/confirm/')
        found = resolve('/confirm/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirm.html')
        self.assertEqual(found.func, confirm)


    def test_contains_question(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("hi! what's poppin?", response_content)

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    
    def test_status_form_validation_for_blank(self):
        new_user = UserForm(data={'user': ''})
        self.assertFalse(new_user.is_valid())
        self.assertEqual(new_user.errors['name'], ["This field is required."])
    
    def test_model_status_return_name_attribute(self):
        new_user = User.objects.create(name='name')
        self.assertEqual(str(new_user), new_user.name)

    def test_model_can_create_new_name(self):
        new_user = User.objects.create(name='name')
        count_all_new_user = User.objects.all().count()
        self.assertEqual(count_all_new_user,1)

    # def test_form_is_valid(self):
    #     response = self.client.post('', data={'user':'User'})
    #     response_content = response.content.decode()
    #     self.assertIn(response_content, 'User')

class story7FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()

    
    def test_landing_page(self):
        selenium = self.driver
        selenium.get(self.live_server_url)
        response_content = self.driver.page_source
        self.assertIn("hi! what's poppin?", response_content)
    
    def test_confirmation_page(self) :
        response = Client().post('', {'name': 'zahra', 'status': 'kinda fine'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirm.html')

    def test_functional(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        # Cancel Button is Clicked
        self.driver.find_element_by_name('name').send_keys('zahra')
        self.driver.find_element_by_name('status').send_keys('kinda fine')
        self.driver.find_element_by_id('submit').click()

        name = self.driver.find_element_by_name('name').get_attribute('value')
        status = self.driver.find_element_by_name('status').get_attribute('value')
        self.assertIn('zahra', name)
        self.assertIn('kinda fine', status)

        self.driver.find_element_by_name('cancel').click()

        self.assertIn('user list', response_content)

        # Submit Button is Clicked
        self.driver.find_element_by_name('name').send_keys('zahra')
        self.driver.find_element_by_name('status').send_keys('kinda fine')
        self.driver.find_element_by_id('submit').click()

        name = self.driver.find_element_by_name('name').get_attribute('value')
        status = self.driver.find_element_by_name('status').get_attribute('value')
        self.assertIn('zahra', name)
        self.assertIn('kinda fine', status)

        self.driver.find_element_by_name('confirm').click()
        
        response_content = self.driver.page_source
        self.assertIn('zahra', response_content)
        self.assertIn('kinda fine', response_content)

        # Check the functionality of the color-changer button
        color_before_time = self.driver.find_element_by_id('response').find_element_by_class_name('statustime').value_of_css_property('color')
        color_before_name = self.driver.find_element_by_id('response').find_element_by_class_name('thename').value_of_css_property('color')
        color_before_status = self.driver.find_element_by_id('response').find_element_by_class_name('thestatus').value_of_css_property('color')
        self.assertEqual('rgba(255, 255, 255, 1)', color_before_time)
        self.assertEqual('rgba(255, 255, 255, 1)', color_before_name)
        self.assertEqual('rgba(255, 255, 255, 1)', color_before_status)

        self.driver.find_element_by_id('change-color').click()

        color_after_time = self.driver.find_element_by_id('response').find_element_by_class_name('statustime').value_of_css_property('color')
        color_after_name = self.driver.find_element_by_id('response').find_element_by_class_name('thename').value_of_css_property('color')
        color_after_status = self.driver.find_element_by_id('response').find_element_by_class_name('thestatus').value_of_css_property('color')
        self.assertEqual('rgba(33, 37, 41, 1)', color_after_time)
        self.assertEqual('rgba(33, 37, 41, 1)', color_after_name)
        self.assertEqual('rgba(33, 37, 41, 1)', color_after_status)